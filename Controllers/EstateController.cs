﻿using Asani.Models;
using Asani.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asani.Controllers
{
    [Route("api/estate")]
    [ApiController]
    public class EstateController : ControllerBase
    {
        private readonly IDataRepository<Estate> _dataRepository;
        public EstateController(IDataRepository<Estate> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Estate> estates = await _dataRepository.GetAll();
            return Ok(estates);
        }

        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(Guid id)
        {
            Estate estate = await _dataRepository.Get(id);
            if (estate == null)
            {
                return NotFound("The Estate record couldn't be found.");
            }
            return Ok(estate);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Estate estate)
        {
            if (estate == null)
            {
                return BadRequest("Estate is null.");
            }
            await _dataRepository.Add(estate);
            return CreatedAtRoute(
                  "Get",
                  new { Id = estate.Id },
                  estate);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Estate estate)
        {
            if (estate == null)
            {
                return BadRequest("Estate is null.");
            }
            Estate employeeToUpdate = await _dataRepository.Get(id);
            if (employeeToUpdate == null)
            {
                return NotFound("The Estate record couldn't be found.");
            }

            await _dataRepository.Update(estate);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Estate estate = await _dataRepository.Get(id);
            if (estate == null)
            {
                return NotFound("The Estate record couldn't be found.");
            }
            await _dataRepository.Delete(estate);
            return NoContent();
        }
    }
}
