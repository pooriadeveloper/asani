﻿using Asani.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asani.Models
{
    public class Estate : BaseEntity
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public EstateType EstateType { get; set; }
        public string Description { get; set; }

        [ForeignKey("Owner")]
        public Guid OwnerId { get; set; }
    }
}
