﻿using Asani.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace TodoApi.Models
{
    public class AsaniDbContext : DbContext
    {
        public AsaniDbContext(DbContextOptions<AsaniDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Owner>().HasData(new Owner
            {
                CreateTime = DateTime.Now,
                CreatorUser = Guid.NewGuid(),
                Firstname = "علی",
                Id = new Guid("AA56DABC-5E57-4F16-BE16-10B1371DF59B"),
                Lastname = "اسدی",
                PhoneNumber = "22334455",
                EditorUser = (Guid?)null,
                EditTime = null
            }, new Owner
            {
                CreateTime = DateTime.Now,
                CreatorUser = Guid.NewGuid(),
                Firstname = "عزیز",
                Id = new Guid("2938C4B9-D430-4995-BB92-9916C9BCB0CD"),
                Lastname = "محمدی",
                PhoneNumber = "22334466",
                EditorUser = (Guid?)null,
                EditTime = null
            });

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Estate> Estates { get; set; }
        public DbSet<Owner> Owners { get; set; }
    }
}