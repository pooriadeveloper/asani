﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asani.Models
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid();
            CreatorUser = Guid.NewGuid();
            CreateTime = DateTime.Now;
            EditorUser = (Guid?)null;
            EditTime = null;
            isDeleted = false;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid CreatorUser { get; set; }
        public Guid? EditorUser { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? EditTime { get; set; }
        public Boolean isDeleted { get; set; }
    }
}
