﻿using Asani.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace Asani.Models.DataManager
{
    public class EntityManager<TEntity> : IDataRepository<TEntity> where TEntity : BaseEntity
    {
        internal AsaniDbContext _entityContext;

        public EntityManager(AsaniDbContext context)
        {
            _entityContext = context;
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            return await _entityContext.Set<TEntity>().Where(current => current.isDeleted == false).ToListAsync();
        }

        public virtual async Task<TEntity> Get(Guid id)
        {
            return await _entityContext.Set<TEntity>()
                  .FirstOrDefaultAsync(e => e.Id == id && e.isDeleted == false);
        }

        public async Task Add(TEntity entity)
        {
            await _entityContext.Set<TEntity>().AddAsync(entity);
            await _entityContext.SaveChangesAsync();
        }

        public async Task Update(TEntity entity)
        {
            TEntity existEntity = await _entityContext.Set<TEntity>().FirstAsync(e => e.Id == entity.Id);
            if (existEntity == null)
            {
                _entityContext.Set<TEntity>().Update(entity);
            }
            else
            {
                entity.EditorUser = new Guid("7AA32679-5226-48E4-9341-EBE632AB7731");
                entity.EditTime = DateTime.Now;
                _entityContext.Entry(existEntity).CurrentValues.SetValues(entity);
            }
            await _entityContext.SaveChangesAsync();
        }

        public async Task Delete(TEntity entity)
        {
            entity.isDeleted = true;
            _entityContext.Set<TEntity>().Update(entity);
            await _entityContext.SaveChangesAsync();
        }
        public async Task Delete(Guid id)
        {
            TEntity entity = await _entityContext.Set<TEntity>().FindAsync(id);
            await Delete(entity);
            await _entityContext.SaveChangesAsync();
        }
    }
}