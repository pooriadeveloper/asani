﻿using System.Collections.Generic;

namespace Asani.Models
{
    public class Owner : BaseEntity
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<Estate> Estates { get; set; }
    }
}
